use std::ops::*;


#[repr(C)]
#[derive(Clone)]
pub struct ClVec3{
    pub x: f32,
    pub y: f32,
    pub z: f32,
    pub _w : f32,
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct ClVec4{
    pub x: f32,
    pub y: f32,
    pub z: f32,
    pub w : f32,
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct ClMat4 (pub [ClVec4; 4]);

impl ClVec4 {
    fn dot(&self, other: &ClVec4) -> f32 {
        self.x * other.x + self.y * other.y + self.z * other.z + self.w * other.w
    }
}

impl ClMat4 {
    // retrieve the i-th column of the matrix and put it in a ClVec4
    fn col(&self, i: usize) -> ClVec4 {
        match i {
            0 => ClVec4{x: self.0[0].x, y: self.0[1].x, z: self.0[2].x, w: self.0[3].x},
            1 => ClVec4{x: self.0[0].y, y: self.0[1].y, z: self.0[2].y, w: self.0[3].y},
            2 => ClVec4{x: self.0[0].z, y: self.0[1].z, z: self.0[2].z, w: self.0[3].z},
            3 => ClVec4{x: self.0[0].w, y: self.0[1].w, z: self.0[2].w, w: self.0[3].w},
            _ => ClVec4{x: 2048.0, y: 2048.0, z: 2048.0, w: 2048.0}
        }
    }

    //retrieve the i-th row of the matrix and put it in a ClVe4
    fn row(&self, i: usize) -> ClVec4 {
        if i <= 3 {
            self.0[i]
        } else {
            ClVec4{x: -2048.0, y: -2048.0, z: -2048.0, w: -2048.0}
        }
    }
}

impl ClMat4 {
    pub fn eulerAngleYXZ(yaw: f32, pitch: f32, roll: f32) -> ClMat4 {
        let ch: f32 = yaw.cos();
        let sh: f32 = yaw.sin();
        let cp: f32 = pitch.cos();
        let sp: f32 = pitch.sin();
        let cb: f32 = roll.cos();
        let sb: f32 = roll.sin();

        ClMat4 ([
            ClVec4{x: ch * cb + sh * sp * sb, y: sb * sp, z: -sh * cb + ch * sp * sb, w: 0.0},
            ClVec4{x:-ch * sb + sh * sp * cb, y: cb * cp, z: sb * sh + ch * sp * cb,  w: 0.0},
            ClVec4{x: sh * cp,                y:-sp,      z:ch * cp,                  w:0.0},
            ClVec4{x:0.0,                     y:0.0,      z:0.0,                      w:1.0},
        ])


    }
    
}

impl Mul for ClMat4 {
    type Output = Self;

    fn mul(self, rhs: Self) -> Self {
        ClMat4( 
            [
                ClVec4{x: self.row(0).dot(&rhs.col(0)), y: self.row(0).dot(&rhs.col(1)), z: self.row(0).dot(&rhs.col(2)), w: self.row(0).dot(&rhs.col(3))},
                ClVec4{x: self.row(1).dot(&rhs.col(0)), y: self.row(1).dot(&rhs.col(1)), z: self.row(1).dot(&rhs.col(2)), w: self.row(1).dot(&rhs.col(3))},
                ClVec4{x: self.row(2).dot(&rhs.col(0)), y: self.row(2).dot(&rhs.col(1)), z: self.row(2).dot(&rhs.col(2)), w: self.row(2).dot(&rhs.col(3))},
                ClVec4{x: self.row(3).dot(&rhs.col(0)), y: self.row(3).dot(&rhs.col(1)), z: self.row(3).dot(&rhs.col(2)), w: self.row(3).dot(&rhs.col(3))},
            ]
        )
    }
    
}

impl ClVec3 {
    pub fn cross(&self, rhs: &ClVec3) -> ClVec3 {
        ClVec3 { x: self.y * rhs.z - self.z * rhs.y , y: self.z * rhs.x - self.x * rhs.z, z: self.x * rhs.y - self.y * rhs.z, _w: 0.0 }
    }
}


