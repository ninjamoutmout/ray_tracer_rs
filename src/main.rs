use std::f32::consts::PI;
use opencl3::*;
use opencl3::{memory::*, kernel::*, program::*, self as cl};
use raylib::{textures::*, prelude::*, self as rlib};
use std::ptr;
mod vec_utils;
use crate::vec_utils::*;

#[repr(C)]
#[derive(Clone)]
pub struct DataBatch{
    camera_matrix: ClMat4,
    fov: f32, // an angle in rad
    fov_scale: f32,
    near_clip_plane: f32,
    aspect: f32
}


#[repr(C)]
pub struct Sphere{
    origin: ClVec3,
    radius: f32,
    material: Material,
}

#[repr(C)]
pub struct Material {
    color: [u32; 4],
}



fn main() -> Result<()>{

    const WINDOW_H: usize = 1080;
    const WINDOW_W: usize = 1920;

    let aspect: f32 = WINDOW_W as f32 / WINDOW_H as f32;

    let mut data: DataBatch = DataBatch {
        fov: PI/3.0,
        fov_scale: (PI / 6.0).tan(),
        near_clip_plane: 1.0,
        camera_matrix: ClMat4([
            // 1 0 0 pos.x
            // 0 1 0 pos.y
            // 0 0 1 pos.z
            // 0 0 0 1
            ClVec4 {x:1.0, y:0.0, z:0.0, w:0.0}, //first col
            ClVec4 {x:0.0, y:1.0, z:0.0, w:0.0}, //second col
            ClVec4 {x:0.0, y:0.0, z:1.0, w:0.0},
            ClVec4 {x:0.0, y:0.0, z:0.0, w:1.0}
            //      ^pos.x ^pos.y ^pos.z
        ]),
        aspect: 16.0/9.0

    };

    let shader_text = std::fs::read_to_string("shader.cl").expect("Couldn't read Shader");
    let device_id = *device::get_all_devices(device::CL_DEVICE_TYPE_GPU).expect("Failed to find devices").first().expect("no");
    let device = device::Device::new(device_id);

    let context = context::Context::from_device(&device).expect("Couldn't create context.");
    let queue = command_queue::CommandQueue::create_default(&context, command_queue::CL_QUEUE_PROFILING_ENABLE).expect("Failed to create command queue");

    let program = Program::create_and_build_from_source(&context, &shader_text, "").expect("Failed to create and build program");
    let kernel = Kernel::create(&program, "render").expect("Failed to create kernel");

    let mut rl = Raylib::init_window(1920, 1080, "Ray tracer rs", 60);
    let img = image::Image::gen_color(&mut rl, 1920, 1080, Color::GREEN);
    let mut gl_texture = texture::Texture::load_from_image(&mut rl, &img).expect("Couldn't generate gl_texture");



    // let mut cam_buf: Buffer<Cam> = unsafe{
        // Buffer::<Cam>::create(&context, memory::CL_MEM_READ_ONLY, 1, ptr::null_mut()).expect("Failed to create Cam Buffer")
    // };
    //
    let cl_texture = unsafe {
        cl::memory::Image::create(
            &context,
            memory::CL_MEM_WRITE_ONLY,
            &cl_image_format {
                image_channel_order: memory::CL_RGBA,
                image_channel_data_type: memory::CL_UNSIGNED_INT8,
            },
            &cl_image_desc{
                image_type: memory::CL_MEM_OBJECT_IMAGE2D,
                image_width: 1920 as usize,
                image_height: 1080 as usize,
                image_depth: 1,
                image_array_size: 1,
                image_row_pitch: 0,
                image_slice_pitch: 0,
                num_mip_levels: 0,
                num_samples: 0,
                buffer: std::ptr::null_mut()
            },
            std::ptr::null_mut()
            ).expect("Failed to create cl texture")
    };

    let mut rendering: bool = true;

    let image_canva = image::Image::gen_color(&mut rl, 1920, 1080, Color::VIOLET);

    let mut yaw: f32 = 0.0;
    let mut pitch: f32 = 0.0;

    let mut old_mouse = rl.get_mouse_pos();

    while !rl.window_should_close() {
        // let old_mouse = rl.get_mouse_pos();
        if rl.is_mouse_button_down(MouseButton::Left){
            let delta = rl.get_mouse_pos() - old_mouse;
            if delta != old_mouse{
                yaw += delta.x / 1000.0;
                pitch += delta.y / 1000.0;
                if pitch >= PI / 4.0 {
                    pitch = PI / 4.0 - 0.01;
                } else if pitch <= -PI/ 4.0 {
                    pitch = - PI / 4.0 +0.01;
                }
                data.camera_matrix = ClMat4::eulerAngleYXZ(yaw, pitch, 0.0) * 
            ClMat4([
                ClVec4 {x:1.0, y:0.0, z:0.0, w:0.0}, //first col
                ClVec4 {x:0.0, y:1.0, z:0.0, w:0.0}, //second col
                ClVec4 {x:0.0, y:0.0, z:1.0, w:0.0},
                ClVec4 {x:0.0, y:0.0, z:0.0, w:1.0}
            ]);
            println!("{:?}", data.camera_matrix);

            }
            rendering = true;

        }
        old_mouse = rl.get_mouse_pos();
        
        rl.begin_drawing(|_, draw|{
            if rendering {
                // let _cam_write_event = unsafe {
                    // queue.enqueue_write_buffer(&mut cam_buf, types::CL_BLOCKING, 0, &[data.clone()], &[]).expect("Failed to write Cam");
                // };

                let kernel_event = unsafe {
                    ExecuteKernel::new(&kernel)
                    .set_arg(&cl_texture)
                    .set_arg(&data)
                    .set_global_work_size(WINDOW_H * WINDOW_W)
                    .enqueue_nd_range(&queue).expect("failed to nd ennqeue range ig")
                };

                let mut events: Vec<types::cl_event> = Vec::default();
                events.push(kernel_event.get());
    
                let image_read = unsafe {
                    queue.enqueue_read_image(&cl_texture, types::CL_NON_BLOCKING, [0, 0, 0].as_ptr(), [1920, 1080, 1].as_ptr(), 0, 0, image_canva.get_ffi_image().data, &events).expect("no read")
                };
                let _ =image_read.wait();
    
    
                let _ = gl_texture.update(&image_canva);
    
    
                        
                rendering = false;
            }

            draw.clear_background(Color::RAYWHITE);
            draw.rectangle(0.0, 0.0, 1920.0, 1080.0, Color::WHITE);
            draw.texture(&gl_texture, 0.0, 0.0, Color::WHITE);

        });
    }


    Ok(())

}
