typedef struct Cam { //size = 96
	float4 camera_mat[4];
	float fov;
	float fov_scale;
	float ncp;
	float aspect;
} Cam;

typedef struct Ray {
	float3 dir;
	float3 origin;
} Ray;

typedef struct Material {
	uint4 color;
} Material;

typedef struct Sphere {
	float3 pos;
	float radius;
	Material material;
} Sphere;

float4 matrix_by_vector(const float4 *m, const float4 v) {
	return (float4
	)(m[0].x * v.x + m[1].x * v.y + m[2].x * v.z + m[3].x * v.w,
	  m[0].y * v.x + m[1].y * v.y + m[2].y * v.z + m[3].y * v.w,
	  m[0].z * v.x + m[1].z * v.y + m[2].z * v.z + m[3].z * v.w,
	  m[0].w * v.x + m[1].w * v.y + m[2].w * v.z + m[3].w * v.w);
}


bool intersect_sphere(const Sphere *sphere, const Ray *ray){
	float3 RayToCenter = sphere->pos - ray->origin;

	float a = dot(ray->dir, ray->dir);
	float b = dot(RayToCenter, ray->dir);
	float c = dot(RayToCenter, RayToCenter) - sphere->radius * sphere->radius;
	float t;
	float disc= b * b - c;
	if (disc < 0.0f)
		return false;
	else
		t = b - sqrt(disc);

	if (t < 0.0f) {
		t = b + sqrt(disc);
		if (t < 0.0f)
			return false;
	}

	return true;

}

uint4 trace(const __private Ray __private *ray) {
	Sphere s;
	s.radius = 1.0;
	s.pos = (float3)(3.0, 0.0, 0.0);
	Sphere s2;
	s2.radius = 9.0;
	s2.pos = (float3)(0.0, -10.0, 0.0);

	if (intersect_sphere(&s, ray)) {
		return (uint4)(255, 0, 0, 255);
	} else if (intersect_sphere(&s2, ray)) {
		return (uint4)(50, 50, 50, 255);
	} else {
		return (uint4)(0, 0, 120, 100);
	}
}




kernel void render (write_only image2d_t ret ,const Cam cam)
{
    const size_t i = get_global_id(0);
	int x = i % 1920;
	int y = trunc(i / 1920.0);

	float xncd = x /1920.0;
	float yncd = y /1080.0;

	float2 screenPos = (float2)((2.f * xncd - 1.f) * cam.aspect * cam.fov_scale, (1.f - 2.f * yncd) * cam.fov_scale);
	float3 camPos = (float3)(screenPos, -cam.ncp);


	Ray ray;
	ray.origin = cam.camera_mat[3].xyz;
	ray.dir = normalize(matrix_by_vector(cam.camera_mat, (float4)(camPos.xyz, 0)).xyz);


	write_imageui(ret, (int2)(x, y), trace(&ray));

}
